import { AdminPage } from "./admin.po";
import { browser, by, element } from "protractor";

describe('minecraft Admin', function () {
  let page: AdminPage;

  beforeEach(() => {
    page = new AdminPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo().then(() => {
      browser.getCurrentUrl().then(data => {
        console.log("current: " + data);
        let elm = element(by.tagName("h2"));
        // elm.then(param => console.log(param));
        expect(elm).toEqual("Панель управления");
      });
    });
  });
});
