import {Component} from "@angular/core";
import {LoadingIndicator} from "../shared/loading.component";
import {IntegrationService} from "../shared/integration.service";

@Component({
    selector: 'app-minecraft-store',
    templateUrl: 'store.component.html'
})
export class StoreComponent extends LoadingIndicator {

    whoami: string;

    constructor(private integrationService: IntegrationService) {
        super(true);
    }

    ngOnInit() {
        this.integrationService
            .whoami()
            .subscribe(data => {
                this.whoami = data;
                this.ready();
            })

    }

}
