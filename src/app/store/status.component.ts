import {Component} from "@angular/core";
import {IntegrationService} from "../shared/integration.service";
import {LoadingIndicator} from "../shared/loading.component";
import {Router, ActivatedRoute} from "@angular/router";
import {DonateModel} from "../shared/donate.model";

@Component({
    selector: 'app-minecraft-status',
    templateUrl: 'status.component.html',
})
export class StatusComponent extends LoadingIndicator {

    donate: DonateModel = new DonateModel();
    statusClass: string = "";
    colorClass: string = "";
    statusItem: string = "unknown";
    statusList = {
        'new': 'Новый',
        'waitAccept': 'Ожидает оплаты',
        'process': 'Обрабатывается',
        'success': 'Оплачен',
        'canceled': 'Отменен',
        'fail': 'Не проведен',
        'unknown': 'Не определен'
    };

    constructor(private integrationService: IntegrationService,
                private activatedRoute: ActivatedRoute,
                private router: Router) {
        super(true);
    }

    ngOnInit() {
        this.activatedRoute
            .queryParams
            .subscribe((data) => {
                this.statusItem = data['ik_inv_st'];
                this.colorClass = 'w3-text-gray';
                this.statusClass = 'fa-question';
                if (['new', 'waitAccept', 'process']
                        .indexOf(this.statusItem) >= 0) {
                    this.statusClass = 'fa-clock-o';
                    this.colorClass = 'w3-text-gray';
                }
                if (['success']
                        .indexOf(this.statusItem) >= 0) {
                    this.statusClass = 'fa-check';
                    this.colorClass = 'w3-text-green';
                }
                if (['canceled', 'fail']
                        .indexOf(this.statusItem) >= 0) {
                    this.statusClass = 'fa-times';
                    this.colorClass = 'w3-text-red';
                }
            });
        this.activatedRoute
            .params
            .subscribe((data) => {
                this.donate.order_id = data['id'];
            });
    }

    onBack() {
        this.router
            .navigate(['/store']);
    }
}