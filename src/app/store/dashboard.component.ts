import {Component} from "@angular/core";
import {IntegrationService} from "../shared/integration.service";
import {ProductModel} from "../shared/product.model";
import {LoadingIndicator} from "../shared/loading.component";
import {Router, ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-minecraft-dashboard',
    templateUrl: 'dashboard.component.html'
})
export class DashboardComponent extends LoadingIndicator {

    productList: ProductModel[] = [];

    constructor(private productService: IntegrationService,
                private activatedRoute: ActivatedRoute,
                private router: Router) {
        super(true);
    }

    ngOnInit() {
        this.productService
            .productList()
            .subscribe(
                list => {
                    this.productList = list;
                    this.ready();
                },
                error => {
                    this.productList = [];
                    this.ready();
                }
            );
    }

    onSelect(product: ProductModel) {
        this.router
            .navigate([product.id, 'donate'], {relativeTo: this.activatedRoute});
    }

}
