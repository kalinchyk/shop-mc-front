import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {DashboardComponent} from "./dashboard.component";
import {DonateComponent} from "./donate.component";
import {StatusComponent} from "./status.component";
import {IntegrationService} from "../shared/integration.service";
import {StoreRoutingModule} from "./store-routing.module";
import {StoreComponent} from "./store.component";

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        StoreRoutingModule
    ],
    declarations: [
        DashboardComponent,
        DonateComponent,
        StatusComponent,
        StoreComponent
    ],
    providers: [
        IntegrationService
    ]
})
export class StoreModule {
}
