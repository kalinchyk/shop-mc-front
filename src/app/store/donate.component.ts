import {Component} from "@angular/core";
import {ProductModel} from "../shared/product.model";
import {IntegrationService} from "../shared/integration.service";
import {LoadingIndicator} from "../shared/loading.component";
import {ActivatedRoute, Params, Router} from "@angular/router";

@Component({
    selector: 'app-minecraft-donate',
    templateUrl: 'donate.component.html'
})
export class DonateComponent extends LoadingIndicator {
    disabled: boolean = false;
    product: ProductModel = new ProductModel();
    user_login: string;
    remember: boolean;
    session_id: string;

    constructor(private integrationService: IntegrationService,
                private activatedRoute: ActivatedRoute,
                private router: Router) {
        super(true);
        this.remember = localStorage.getItem("remember") != "0";
        this.user_login = this.remember ? localStorage.getItem("user_login") : "";
        this.session_id = localStorage.getItem("session_id");
    }

    ngOnInit() {
        this.activatedRoute
            .params
            .switchMap((params: Params) => {
                return this.integrationService.productItem(params['id'])
            })
            .subscribe((product: ProductModel) => {
                this.product = product;
                this.ready();
            }, error => console.log(error));
    }

    donate() {
        this.standby();
        if (this.disabled) return;
        this.disabled = true;
        localStorage.setItem("remember", this.remember ? "1" : "0");
        localStorage.setItem("user_login", this.remember ? this.user_login : "");
        this.integrationService
            .donate(this.user_login, this.product.id, this.session_id)
            .subscribe((data) => {
                this.session_id = data.session_id;
                localStorage.setItem("session_id", this.session_id);
                let frm = document.createElement('form');
                frm.action = data.form.action;
                frm.method = data.form.method;
                Object.keys(data.form.list)
                    .forEach((item) => {
                        let elm = document.createElement('input');
                        elm.type = 'hidden';
                        elm.name = item;
                        elm.value = data.form.list[item];
                        frm.appendChild(elm);
                    });
                document.body.appendChild(frm);
                frm.submit();
            }, (error) => {
                this.disabled = false;
                this.ready();
            });
    }

    onBack() {
        this.router
            .navigate(['/store']);
    }
}