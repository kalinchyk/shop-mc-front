import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {DashboardComponent} from "./dashboard.component";
import {DonateComponent} from "./donate.component";
import {StatusComponent} from "./status.component";
import {StoreComponent} from "./store.component";

const storeRoutes: Routes = [
    {
        path: '',
        component: StoreComponent,
        children: [{
            path: '',
            component: DashboardComponent
        }, {
            path: ':id/donate',
            component: DonateComponent
        }, {
            path: ':id/status',
            component: StatusComponent
        }]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(storeRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class StoreRoutingModule {
}
