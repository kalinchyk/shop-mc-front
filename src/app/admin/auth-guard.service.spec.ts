/* tslint:disable:no-unused-variable */
import {TestBed, inject} from "@angular/core/testing";
import {AuthGuardService} from "./auth-guard.service";
import {RouterTestingModule} from "@angular/router/testing";
import {AuthService} from "./auth.service";
import { HttpModule } from "@angular/http";

describe('AuthGuardService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule, HttpModule],
            providers: [AuthGuardService, AuthService],
        });
    });

    it('should ...', inject([AuthGuardService], (service: AuthGuardService) => {
        expect(service).toBeTruthy();
    }));
});
