import {Component} from "@angular/core";
import {LoadingIndicator} from "../../shared/loading.component";
import {ConsoleService} from "../shared/console.service";
@Component({
    templateUrl: 'console.component.html'
})
export class ConsoleComponent extends LoadingIndicator {
    command: string;
    response: string = 'Добро пожаловать в консоль администратора';

    constructor(private consoleService: ConsoleService) {
        super(true);
    }

    ngOnInit() {
        this.ready()
    }

    send() {
        this.consoleService
            .send(this.command)
            .subscribe(data => this.response = data);
    }
}