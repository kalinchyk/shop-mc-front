import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {AuthGuardService} from "./auth-guard.service";
import {AdminComponent} from "./admin.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {ProductListComponent} from "./product/product-list.component";
import {ProductHistoryComponent} from "./product/product-history.component";
import {ProductDetailComponent} from "./product/product-detail.component";
import {ProductComponent} from "./product/product.component";
import {DonationListComponent} from "./donation/donation-list.component";
import {DonationComponent} from "./donation/donation.component";
import {DonationDetailComponent} from "./donation/donation-detail.component";
import {ConsoleComponent} from "./console/console.component";

const adminRoutes: Routes = [
    {
        path: '',
        component: AdminComponent,
        canActivate: [
            AuthGuardService
        ],
        children: [
            {
                path: '',
                canActivateChild: [
                    AuthGuardService
                ],
                children: [
                    {
                        path: 'console',
                        component: ConsoleComponent
                    },
                    {
                        path: 'product',
                        component: ProductComponent,
                        children: [
                            {
                                path: '',
                                component: ProductListComponent
                            },
                            {
                                path: 'create',
                                component: ProductDetailComponent
                            },
                            {
                                path: ':id/history',
                                component: ProductHistoryComponent
                            },
                            {
                                path: ':id',
                                component: ProductDetailComponent
                            }
                        ]
                    },
                    {
                        path: 'donation',
                        component: DonationComponent,
                        children: [
                            {
                                path: '',
                                component: DonationListComponent
                            },
                            {
                                path: ':id',
                                component: DonationDetailComponent
                            }
                        ]
                    },
                    {
                        path: 'dashboard',
                        component: DashboardComponent
                    },
                    {
                        path: '',
                        redirectTo: 'dashboard',
                        pathMatch: 'full'
                    }
                ]
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(adminRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AdminRoutingModule {
}
