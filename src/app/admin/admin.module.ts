import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {AdminRoutingModule} from "./admin-routing.module";
import {AdminComponent} from "./admin.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {FormsModule} from "@angular/forms";
import {AuthService} from "./auth.service";
import {ProductListComponent} from "./product/product-list.component";
import {ProductDetailComponent} from "./product/product-detail.component";
import {ProductHistoryComponent} from "./product/product-history.component";
import {ProductComponent} from "./product/product.component";
import {ProductService} from "./shared/product.service";
import {ColorService} from "./shared/color.service";
import {DonationComponent} from "./donation/donation.component";
import {DonationListComponent} from "./donation/donation-list.component";
import {DonationDetailComponent} from "./donation/donation-detail.component";
import {DonationService} from "./shared/donation.service";
import {ProductHistoryService} from "./shared/product-history.service";
import {ConsoleComponent} from "./console/console.component";
import {ConsoleService} from "./shared/console.service";

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        AdminRoutingModule
    ],
    declarations: [
        AdminComponent,
        ConsoleComponent,
        DashboardComponent,
        ProductComponent,
        ProductListComponent,
        ProductDetailComponent,
        ProductHistoryComponent,
        DonationComponent,
        DonationListComponent,
        DonationDetailComponent
    ],
    providers: [
        AuthService,
        ColorService,
        ConsoleService,
        ProductService,
        DonationService,
        ProductHistoryService
    ]
})
export class AdminModule {
}
