import {Component, OnInit} from "@angular/core";
import {AuthService} from "./auth.service";
import {LoadingIndicator} from "../shared/loading.component";

@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html'
})
export class AdminComponent extends LoadingIndicator {

    constructor(private authService: AuthService) {
        super(true);
    }

    ngOnInit() {
        this.ready();
    }

    logout() {
        // console.log("DashboardComponent#logout");
        this.authService
            .logout()
            .subscribe();
    }

}
