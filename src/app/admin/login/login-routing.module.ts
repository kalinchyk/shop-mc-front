import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {AuthGuardService} from "../auth-guard.service";
import {LoginComponent} from "./login.component";
import {AuthService} from "../auth.service";

const routes: Routes = [{
    path: 'login',
    component: LoginComponent
}];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
        AuthService,
        AuthGuardService
    ]
})
export class LoginRoutingModule {
}
