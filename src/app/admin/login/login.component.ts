import {Component} from "@angular/core";
import {AuthService} from "../auth.service";
import {Router} from "@angular/router";
import {LoadingIndicator} from "../../shared/loading.component";

@Component({
    selector: 'app-login',
    templateUrl: 'login.component.html'
})
export class LoginComponent extends LoadingIndicator {
    username: string = "root";
    password: string = "toor";

    constructor(private router: Router,
                private authService: AuthService) {
        super(true);
    }

    ngOnInit() {
        this.ready();
    }

    login() {
        this.authService
            .login(this.username, this.password)
            .subscribe();
    }

    onBack() {
        this.router
            .navigate(["/"]);
    }

}
