import {Component} from "@angular/core";
import {LoadingIndicator} from "../../shared/loading.component";
import {Router, ActivatedRoute} from "@angular/router";
import {DonationService} from "../shared/donation.service";
import {DonationModel} from "../shared/donation.model";
@Component({
    templateUrl: "donation-list.component.html"
})
export class DonationListComponent extends LoadingIndicator {

    private donationList: DonationModel[] = [];
    private listLoad: boolean = false;

    ngOnInit(): void {
        this.reload();
    }

    reload() {
        this.standby();
        this.donationList = [];
        this.listLoad = true;
        this.donationService
            .donationList()
            .subscribe((list) => {
                this.listLoad = false;
                this.donationList = list;
                this.ready();
            });
    }

    constructor(private router: Router,
                private donationService: DonationService,
                private activatedRoute: ActivatedRoute) {
        super(true);
    }

    detail(donation: DonationModel) {
        this.standby();
        this.router
            .navigate([donation.id], {relativeTo: this.activatedRoute});
    }

}