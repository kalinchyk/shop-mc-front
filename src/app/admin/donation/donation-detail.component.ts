import {Component} from "@angular/core";
import {LoadingIndicator} from "../../shared/loading.component";
import {ActivatedRoute, Params} from "@angular/router";
import {DonationService} from "../shared/donation.service";
import {DonationModel} from "../shared/donation.model";
@Component({
    templateUrl: "donation-detail.component.html"
})
export class DonationDetailComponent extends LoadingIndicator {

    private donation: DonationModel = new DonationModel();

    constructor(private activatedRoute: ActivatedRoute,
                private donationService: DonationService) {
        super(true);
    }

    ngOnInit() {
        this.activatedRoute
            .params
            .switchMap((params: Params) => {
                return this.donationService.donationItem(params['id'])
            })
            .subscribe((donation: DonationModel) => {
                this.donation = donation;
                this.ready();
            }, error => console.log(error));
    }


}