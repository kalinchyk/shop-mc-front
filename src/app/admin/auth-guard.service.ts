import {Injectable} from "@angular/core";
import {
    CanLoad,
    CanActivateChild,
    CanActivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    Route,
    Router
} from "@angular/router";
import {Observable} from "rxjs";
import {AuthService} from "./auth.service";

@Injectable()
export class AuthGuardService implements CanActivate, CanActivateChild, CanLoad {

    canActivate(route: ActivatedRouteSnapshot,
                state: RouterStateSnapshot): Observable<boolean>|Promise<boolean>|boolean {
        return this.checkLogin(state.url);
    }

    canActivateChild(childRoute: ActivatedRouteSnapshot,
                     state: RouterStateSnapshot): Observable<boolean>|Promise<boolean>|boolean {
        return this.canActivate(childRoute, state);
    }

    canLoad(route: Route): Observable<boolean>|Promise<boolean>|boolean {
        return this.checkLogin(`/${route.path}`);
    }

    constructor(private router: Router,
                private authService: AuthService) {
    }

    checkLogin(url: string): Observable<boolean>|Promise<boolean>|boolean {
        if (this.authService.isLoggedIn) return true;
        this.authService.redirectUrl = url;
        if (this.authService.isLoggedIn == null) {
            return this.authService
                .check();
        } else {
            this.router
                .navigate(['/login'])
            return false;
        }
    }

}
