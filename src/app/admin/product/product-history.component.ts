import {Component} from "@angular/core";
import {LoadingIndicator} from "../../shared/loading.component";
import {ProductHistoryModel} from "../shared/product-history.model";
import {ProductHistoryService} from "../shared/product-history.service";
import {ActivatedRoute, Params} from "@angular/router";

@Component({
    selector: 'admin-product-history',
    templateUrl: 'product-history.component.html'
})
export class ProductHistoryComponent extends LoadingIndicator {

    productId: number;
    productHistory: ProductHistoryModel = new ProductHistoryModel();
    productHistoryList: ProductHistoryModel[] = [];

    constructor(private productHistoryService: ProductHistoryService,
                private activatedRoute: ActivatedRoute) {
        super(true);
    }

    ngOnInit() {
        this.activatedRoute
            .params
            .switchMap(
                (params: Params) => {
                    this.productId = params['id'];
                    return this.productHistoryService
                        .productHistoryList(this.productId)
                }
            )
            .subscribe(
                (productHistoryList: ProductHistoryModel[]) => {
                    this.productHistoryList = productHistoryList;
                    this.productHistory = this.productHistoryList.length == 0
                        ? new ProductHistoryModel() : this.productHistoryList[0];
                    this.ready();
                },
                error =>
                    console.log(error)
            );
    }


}
