import {Component} from "@angular/core";
import {LoadingIndicator} from "../../shared/loading.component";
import {ProductModel} from "../../shared/product.model";
import {ProductService} from "../shared/product.service";
import {Router, ActivatedRoute} from "@angular/router";

@Component({
    selector: 'admin-product-list',
    templateUrl: 'product-list.component.html'
})
export class ProductListComponent extends LoadingIndicator {

    private productList: ProductModel[] = [];
    private listLoad: boolean = false;

    constructor(private router: Router,
                private productService: ProductService,
                private activatedRoute: ActivatedRoute) {
        super(true);
    }

    ngOnInit() {
        this.reload()
    }

    reload() {
        this.standby();
        this.productList = [];
        this.listLoad = true;
        this.productService
            .productList()
            .subscribe((list) => {
                this.listLoad = false;
                this.productList = list;
                this.ready();
            });
    }

    detail(product: ProductModel) {
        this.standby();
        this.router
            .navigate([product.id], {relativeTo: this.activatedRoute});
    }

}
