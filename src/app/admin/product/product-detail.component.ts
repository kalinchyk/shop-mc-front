import {Component, OnInit} from "@angular/core";
import {LoadingIndicator} from "../../shared/loading.component";
import {ProductService} from "../shared/product.service";
import {ProductModel} from "../shared/product.model";
import {Params, ActivatedRoute, Router} from "@angular/router";
import {ColorService} from "../shared/color.service";
import {ColorModel} from "../shared/color.model";

@Component({
    selector: 'admin-product-detail',
    templateUrl: 'product-detail.component.html'
})
export class ProductDetailComponent extends LoadingIndicator implements OnInit {
    private isModify: boolean;
    private colorList: ColorModel[] = [];
    private product: ProductModel = new ProductModel();

    ngOnInit(): void {
        this.colorService
            .colorList()
            .subscribe(
                (colorList: ColorModel[]) => {
                    this.colorList = colorList;
                }
            );
        if (this.isModify) {
            this.activatedRoute
                .params
                .switchMap(
                    (params: Params) => {
                        return this.productService
                            .productItem(params['id'])
                    }
                )
                .subscribe(
                    (product: ProductModel) => {
                        this.product = product;
                        this.ready();
                    },
                    error =>
                        console.log(error)
                );
        } else {
            this.ready();
        }
    }

    constructor(private router: Router,
                private activatedRoute: ActivatedRoute,
                private colorService: ColorService,
                private productService: ProductService) {
        super(true);
        this.isModify = !router.url.endsWith("/create");
    }

    save() {
        this.standby();
        this.productService
            .productSave(this.product)
            .subscribe(() => {
                this.ready();
            });
    }

    delete(product: ProductModel) {
        this.standby();
        this.productService
            .productDelete(product)
            .subscribe(() => {
                this.router
                    .navigate(['../'], {relativeTo: this.activatedRoute});
            });
    }

}
