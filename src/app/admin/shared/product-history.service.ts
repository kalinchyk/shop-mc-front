import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs";
import {ProductHistoryModel} from "./product-history.model";
import {Router} from "@angular/router";

@Injectable()
export class ProductHistoryService {

    constructor(private http: Http, private router: Router) {

    }

    productHistoryList(productId: number): Observable<ProductHistoryModel[]> {
        return this.http
            .get(`api/private/product/${productId}/history`)
            .map(
                response =>
                (response.json() || {"list": []}).list || []
            )
            .catch(
                (err) => {
                    if (err instanceof Response) {
                        if (err.status == 401)
                            this.router.navigate(['/login']);
                    }
                    return Observable.throw(err);
                }
            );
    }
}