import {ProductHistoryModel} from "./product-history.model";
export class DonationModel {
    id: number;
    user_login: string;
    status: string;
    product_history: ProductHistoryModel = new ProductHistoryModel();

    constructor() {

    }
}