export class ResponseModel {
    success: boolean;
    message: string;
    stack: any;
}
