import {ProductModel} from "./product.model";
import {ActionModel} from "./action.model";

export class ProductHistoryModel extends ProductModel {
    product_id: number;
    date: string;
    action: ActionModel = new ActionModel();

    constructor() {
        super();
    }
}
