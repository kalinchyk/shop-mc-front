import {ColorModel} from "./color.model";
export class ProductModel {
    id?: number;
    name?: string;
    description?: string;
    detail?: string;
    amount?: number;
    color?: ColorModel = new ColorModel();
    command?: string;

    constructor(){
    }
}
