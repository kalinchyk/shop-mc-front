import {Injectable} from "@angular/core";
import {ProductModel} from "./product.model";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs";
import {Router} from "@angular/router";
import {ResponseModel} from "./response.model";

@Injectable()
export class ProductService {

    constructor(private http: Http, private router: Router) {
    }

    productList(): Observable<ProductModel[]> {
        return this.http
            .get(`api/private/product/list`)
            .map(
                response =>
                (response.json() || {"list": []}).list || []
            )
            .catch(
                (err) => {
                    if (err instanceof Response) {
                        if (err.status == 401)
                            this.router.navigate(['/login']);
                    }
                    return Observable.throw(err);
                }
            );
    }

    productItem(id: number | string): Observable<ProductModel> {
        return this.http
            .get(`api/private/product/${id}`)
            .map(
                response =>
                response.json() || {}
            )
            .catch(
                (err) => {
                    if (err instanceof Response) {
                        if (err.status == 401)
                            this.router.navigate(['/login']);
                    }
                    return Observable.throw(err);
                }
            );
    }

    productSave(product: ProductModel): Observable<ResponseModel> {
        return (product.id == null
            ? this.http.post(`api/private/product`, product)
            : this.http.put(`api/private/product/${product.id}`, product))
            .map(
                (data: Response) => {
                    let response = new ResponseModel();
                    response.success = data.status == 200;
                    response.message = data.json().message;
                    if (data.json().rowid)
                        this.router.navigate(['/admin/product', data.json().rowid]);
                    return Observable.of(response);
                }
            )
            .catch(
                (data: Response | any) => {
                    let response = new ResponseModel();
                    response.success = false;
                    response.message = (data instanceof Response) ? data.json().message : data.message;
                    response.stack = data.stack || null;
                    return Observable.of(response);
                }
            );
    }

    productDelete(product: ProductModel): Observable<ResponseModel> {
        return this.http
            .delete(`api/private/product/${product.id}`)
            .map(
                (data: Response) => {
                    let response = new ResponseModel();
                    response.success = data.status == 200;
                    response.message = data.json().message;
                    return Observable.of(response);
                }
            )
            .catch(
                (data: Response | any) => {
                    let response = new ResponseModel();
                    response.success = false;
                    response.message = (data instanceof Response) ? data.json().message : data.message;
                    response.stack = data.stack || null;
                    return Observable.of(response);
                }
            );
    }

}
