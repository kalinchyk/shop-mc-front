import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs";

@Injectable()
export class ConsoleService {
    constructor(private http: Http) {

    }

    send(command: string): Observable<any> {
        return this.http
            .post(`api/private/server/command`, {"command": command})
            .map((response: Response) => response.text())
    }
}