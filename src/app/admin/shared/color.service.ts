import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs";
import {ColorModel} from "./color.model";
import {Router} from "@angular/router";

@Injectable()
export class ColorService {

    constructor(private http: Http,
                private router: Router) {
    }

    colorList(): Observable<ColorModel[]> {
        return this.http
            .get(`api/private/color/list`)
            .map(
                response =>
                (response.json() || {"list": []}).list || []
            )
            .catch(
                (err) => {
                    if (err instanceof Response) {
                        if (err.status == 401)
                            this.router.navigate(['/login']);
                    }
                    return Observable.throw(err);
                }
            );
    }

}
