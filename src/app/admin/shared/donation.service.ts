import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {DonationModel} from "./donation.model";
import {Http, Response} from "@angular/http";
import {Router} from "@angular/router";

@Injectable()
export class DonationService {

    constructor(private router: Router,
                private http: Http) {
    }

    donationList(): Observable<DonationModel[]> {
        return this.http
            .get(`api/private/donate/list`)
            .map(
                response =>
                (response.json() || {"list": []}).list || [] as DonationModel[]
            )
            .catch(
                (err) => {
                    if (err instanceof Response) {
                        if (err.status == 401)
                            this.router.navigate(['/login']);
                    }
                    return Observable.throw(err);
                }
            );
    }

    donationItem(id: number | string): Observable<DonationModel> {
        return this.http
            .get(`api/private/donate/${id}`)
            .map(
                response =>
                response.json() || {}
            )
            .catch(
                (err) => {
                    if (err instanceof Response) {
                        if (err.status == 401)
                            this.router.navigate(['/login']);
                    }
                    return Observable.throw(err);
                }
            );
    }
}