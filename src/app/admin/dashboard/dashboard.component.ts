import {Component, OnInit} from "@angular/core";
import {LoadingIndicator} from "../../shared/loading.component";

@Component({
    selector: 'admin-home',
    templateUrl: 'dashboard.component.html'
})
export class DashboardComponent extends LoadingIndicator {

    constructor() {
        super(true);
    }

    ngOnInit() {
        this.ready()
    }


}
