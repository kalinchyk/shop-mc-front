import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Http, Response} from "@angular/http";
import {Router} from "@angular/router";

@Injectable()
export class AuthService {
    isLoggedIn: boolean = null;
    redirectUrl: string;

    constructor(private http: Http, private router: Router) {
    }

    check(): Observable<boolean> {
        return this.http
            .post(`api/auth/ping`, null)
            .map((data: Response | any) => {
                return (data.status == 200);
            })
            .catch((data: Response | any) => {
                return Observable.of(false)
            })
            .do((data: boolean) => {
                this.isLoggedIn = data;
                if (!data)
                    this.router
                        .navigate(['/login']);
            });
    }

    login(username: string, password: string): Observable<any> {
        return this.http
            .post(`api/auth/login`, {"username": username, "password": password})
            .map((data: Response | any) => {
                return {"success": data.status == 200, "message": data.json().message, "stack": null}
            })
            .catch((data: Response | any) => {
                return Observable.of({
                    "success": false,
                    "message": (data instanceof Response) ? data.json().message : data.message,
                    "stack": data.stack || null
                })
            })
            .do((data) => {
                console.log(data);
                this.isLoggedIn = data.success;
                if (data.success) {
                    this.router
                        .navigate([this.redirectUrl ? this.redirectUrl : "/admin/"]);
                    this.redirectUrl = null;
                }
            });
    }


    logout(): Observable<boolean> {
        return this.http
            .post(`api/auth/logout`, null)
            .catch((data: Response | any) => {
                return Observable.of(false)
            })
            .do((data) => {
                this.isLoggedIn = false;
                this.router
                    .navigate(['/login']);
            });
    }

}
