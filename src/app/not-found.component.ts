import {Component} from "@angular/core";
import {LoadingIndicator} from "./shared/loading.component";

@Component({
    selector: 'app-page-not-found',
    templateUrl: 'not-found.component.html'
})
export class PageNotFoundComponent extends LoadingIndicator {

    constructor() {
        super(true);
    }

    ngOnInit() {
        this.ready();
    }

}
