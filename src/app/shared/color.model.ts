export class ColorModel {
    id?: number;
    code?: string;
    name?: string;
}
