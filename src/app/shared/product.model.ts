import {ColorModel} from "./color.model";

export class ProductModel {
    id?: number;
    name?: string;
    desc?: string;
    description?: string;
    amount?: number;
    amount_round?: string;
    amount_fract?: string;
    color?: ColorModel = new ColorModel();
}
