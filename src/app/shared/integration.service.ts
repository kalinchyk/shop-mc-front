import {Injectable} from "@angular/core";
import {ProductModel} from "./product.model";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs";
import {ColorModel} from "./color.model";
import {DonateModel} from "./donate.model";

@Injectable()
export class IntegrationService {

    constructor(private http: Http) {
    }

    colorList(): Observable<ColorModel[]> {
        return this.http
            .get(`api/color/list`)
            .map(response => (response.json() || {"list": []}).list || [])
            .catch(this.handleError);
    }

    productList(): Observable<ProductModel[]> {
        return this.http
            .get(`api/product/list`)
            .map(response => {
                let result = (response.json() || {"list": []}).list || [];
                result = result.map((product: ProductModel) => {
                    [product.amount_round, product.amount_fract] = product.amount.toString().split(".");
                    return product;
                });
                return result;
            })
            .catch(this.handleError);
    }

    productItem(id: number | string): Observable<ProductModel> {
        return this.http
            .get(`api/product/${id}`)
            .map(response => response.json() || {})
            .catch(this.handleError);
    }

    donate(login: string, productId: number, session_id: string): Observable<any> {
        if (session_id === "undefined") session_id = "";
        return this.http
            .post(`api/donate`, {
                "user_login": login,
                "product_id": productId,
                "session_id": session_id
            })
            .map(response => response.json() || {})
            .catch(this.handleError)
    }

    donateItem(id: number | string): Observable<DonateModel> {
        return this.http
            .get(`api/donate/${id}`)
            .map(response => response.json() || {})
    }

    private handleError(err: Response | any) {
        return Observable.throw(err);
    }

    whoami(): Observable<string> {
        return this.http
            .get(`api/whoami`)
            .map(response => (response.json() || {}).object || "CUSTOMER")
            .catch(this.handleError)
    }

}
