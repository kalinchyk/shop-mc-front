export class DonateModel {
    id: number;
    order_id: string;
    user_login: string;
    date: number;
    status: string;
}