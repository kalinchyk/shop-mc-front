import { ResponseModel } from "../admin/shared/response.model";
export class LoadingIndicator {

  public loading: boolean;

  constructor(val: boolean) {
    val ? this.standby() : this.ready();
  }

  standby(): void {
    try {
      document.getElementById("loading").style.display = "block";
    } catch (Exception) {

    }

  }

  ready(): void {
    try {
      document.getElementById("loading").style.display = "none";
    } catch (Exception) {

    }

  }

  error(response: ResponseModel): void {
    try {
      document.getElementById("error-modal").style.display = response.success ? "none" : "block";
      document.getElementById("error-message").innerText = response.message;
      document.getElementById("error-stack").innerText = response.stack;
    } catch (Exception) {

    }
  }
}