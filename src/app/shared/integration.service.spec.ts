/* tslint:disable:no-unused-variable */
import { TestBed, inject } from "@angular/core/testing";
import { IntegrationService } from "./integration.service";
import { BaseRequestOptions, Http, ResponseOptions, Response } from "@angular/http";
import { MockBackend, MockConnection } from "@angular/http/testing";
import { ProductModel } from "./product.model";

describe('IntegrationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: Http,
          useFactory: (backend, options) => {
            return new Http(backend, options);
          },
          deps: [
            MockBackend,
            BaseRequestOptions
          ]
        },
        MockBackend,
        BaseRequestOptions,
        IntegrationService
      ]
    });
  });

  it('should ...', inject([IntegrationService, MockBackend], (service: IntegrationService, mockBackend: MockBackend) => {
    mockBackend.connections.subscribe((conn: MockConnection) => {
      const options: ResponseOptions = new ResponseOptions({
        body: {
          "object": "product",
          "list": [
            {
              "color": "red",
              "amount": "1.99",
              "desc": "\u0420\u0430\u0437\u0431\u043b\u043e\u043a\u0438\u0440\u043e\u0432\u0430\u0442\u044c \u0443\u0447\u0435\u0442\u043d\u0443\u044e \u0437\u0430\u043f\u0438\u0441\u044c",
              "id": 1,
              "name": "unBan"
            },
            {
              "color": "green",
              "amount": "10.99",
              "desc": "\u043f\u043e\u043b\u0435\u0442",
              "id": 2,
              "name": "fly"
            }
          ]
        }
      });
      conn.mockRespond(new Response(options));
    });
    service.productList()
      .subscribe((res: ProductModel[]) => {
        expect(res.length).toEqual(2);
        expect(res[0].id).toEqual(1);
        expect(res[1].id).toEqual(2);
      });
  }));
});
