import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {PageNotFoundComponent} from "./not-found.component";
import {AuthGuardService} from "./admin/auth-guard.service";

const appRoutes: Routes = [
    {
        path: 'store',
        loadChildren: 'app/store/store.module#StoreModule'
    },
    {
        path: 'admin',
        loadChildren: 'app/admin/admin.module#AdminModule',
        canLoad: [
            AuthGuardService
        ]
    },
    {
        path: '',
        redirectTo: '/store',
        pathMatch: 'full'
    },
    {
        path: '**',
        component: PageNotFoundComponent
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {
}
