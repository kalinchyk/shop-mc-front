import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {AppComponent} from "./app.component";
import {PageNotFoundComponent} from "./not-found.component";
import {AppRoutingModule} from "./app-routing.module";
import {AuthGuardService} from "./admin/auth-guard.service";
import {AuthService} from "./admin/auth.service";
import {IntegrationService} from "./shared/integration.service";
import {LoginRoutingModule} from "./admin/login/login-routing.module";
import {LoginComponent} from "./admin/login/login.component";

@NgModule({
    declarations: [
        AppComponent,
        PageNotFoundComponent,
        LoginComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        LoginRoutingModule,
        AppRoutingModule
    ],
    providers: [
        AuthGuardService,
        AuthService,
        IntegrationService
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
}
